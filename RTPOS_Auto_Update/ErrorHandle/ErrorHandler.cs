﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTPOS_Auto_Update.ErrorHandle
{
    public class ErrorHandler
    {
        public bool EnableLogging = true;

        private static ErrorHandler _this = null;

        private static int ErrorCount = 0;

        public static ErrorHandler Instance
        {
            get
            {
                if (_this == null)
                    _this = new ErrorHandler();

                ErrorCount++;

                return _this;
            }
        }

        public int GetErrorCount()
        {
            return ErrorCount;
        }

        public void ExceptionedError(string caption, ErrorCode errorCode, Exception e)
        {
            if (EnableLogging)
            {
                Program.Log(string.Format("Error (#{3}) => {0}({2}): - {1}", 
                    caption, e.Message, errorCode.ToString(), ErrorCount));
            }
            MessageBox.Show(null, caption, string.Format("{0} - {1}", errorCode.ToString(), e.Message), 
                MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
        }

        public DialogResult ErrorOption(string caption, string message, 
            ErrorCode errorCode, MessageBoxButtons buttons = MessageBoxButtons.YesNoCancel, MessageBoxIcon icon = MessageBoxIcon.Warning)
        {
            if (EnableLogging)
            {
                var result = MessageBox.Show(null, caption, string.Format("{0} - {1}", errorCode.ToString(), message),
                buttons, icon);

                Program.Log(string.Format("Error (#{3}) => {0}({2}): - {1} | Result: {3}",
                    caption, message, errorCode.ToString(), result.ToString(), ErrorCount));

                return result;
            }
            return MessageBox.Show(null, caption, string.Format("{0} - {1}", errorCode.ToString(), message),
                buttons, icon);
        }
        

        public void Error(string caption, string message, ErrorCode errorCode)
        {
            if (EnableLogging)
            {
                Program.Log(string.Format("Error  (#{3}) => {0}({2}): - {1}",
                    caption, message, errorCode.ToString(), ErrorCount));
            }
            MessageBox.Show(null, caption, string.Format("{0} - {1}", errorCode.ToString(), message),
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }


    }
}
