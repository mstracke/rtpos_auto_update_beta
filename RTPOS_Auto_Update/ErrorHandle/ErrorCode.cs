﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.ErrorHandle
{
    public enum ErrorCode : int
    {
        NO_ERROR = 0,
        ERROR_SUCCESS,
        WARNING_NO_ERROR,
        NET_EXCEPTION,
        WEB_EXCEPTION,
        SELF_UPDATE_FAILURE,
        POS_UPDATE_FAILURE,
        EXEC_EXCEPTION,
        WEB_CANCELED

    }
}
