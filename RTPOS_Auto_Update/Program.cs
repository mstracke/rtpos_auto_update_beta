﻿using RTPOS_Auto_Update.UpdateEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using RTPOS_Auto_Update.UpdateEngine.Events;
using RTPOS_Auto_Update.ErrorHandle;
using System.Runtime.InteropServices;
using RTPOS_Auto_Update.Logging;

namespace RTPOS_Auto_Update
{
    static class Program
    {

        public static readonly string ExecutingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);        
        public static volatile string CURRENT_PROCESS_NAME = Process.GetCurrentProcess().ProcessName;
        public const string UPDATED_PROCESS_NAME = "RTPOSAutoUpdated";
        private static List<IDisposable> DisposeList = new List<IDisposable>();
        public const string NORM_PROCESS_NAME = "RTPOSAutoUpdate.exe";
        private static string StoreID = null;
        private static string AuthString = null;
        private static List<string> commands = new List<string>();
        private static List<string> options = new List<string>();
        public static bool HasAuth { get; set; }
        public static bool HasStoreID { get; set; }
        public static string[] CurrentCmdArgs { get; private set; }
        const UpdateMode DEFAULT_UPDATEMODE = UpdateMode.EXE;
        public static UpdateMode CurrentUpdateMode = DEFAULT_UPDATEMODE;
        private static bool IsStarted = false;

        private static Logger logger;

        [STAThread]
        static void Main(string[] args)
        {

            HandleArgs(args);
            logger = new Logger(URLS.POS_LOG_FILE_NAME);
            SelfUpdater.Instance.SelfUpdateEvent += Instance_SelfUpdateEvent;
            SelfUpdater.Instance.UpdateSelf(CURRENT_PROCESS_NAME, CurrentUpdateMode);
            
        }

        internal static void Log(string msg)
        {
            logger.Log(msg);
        }

        internal static string GetStoreID()
        {
            return StoreID;
        }

        internal static string GetAuth()
        {
            return AuthString;
        }

        internal static void ToDispose(IDisposable item)
        {
            DisposeList.Add(item);
        }

        private static void Instance_SelfUpdateEvent(object sender, RenNetEventArgs args)
        {
            
            if (args.Result == REN_Result.SUCCESS)
            {
                //Rename and restart
                SelfUpdater.Instance.RenameSelf(UPDATED_PROCESS_NAME + ".exe", NORM_PROCESS_NAME);
            }
            else if(args.Result == REN_Result.SUCCESS_NO_UPDATE)
            {
                SelfUpdater.Instance.SelfUpdateEvent -= Instance_SelfUpdateEvent;
                //Executer will continue
            }
            else if(args.Result == REN_Result.NEEDS_UPDATE)
            {
                //Add the self updater flag
                CurrentUpdateMode = CurrentUpdateMode | UpdateMode.UPDATE_SELF;
                StartMainApplication();
            }
            else
            {
                //Somehow this part of Self Update failed but we can still try to update rtpos via reinstall, 
                //which can include latest updater/fix issue.
                SelfUpdater.Instance.SelfUpdateEvent -= Instance_SelfUpdateEvent;
                CurrentUpdateMode = UpdateMode.REINSTALL;

                if(ErrorHandler.Instance.ErrorOption("Self Update Failed", 
                    "AutoUpdate Failed to Update Self, Click Yes to try and update RTPOS by reinstalling, or No to Exit.", 
                    ErrorCode.SELF_UPDATE_FAILURE) == DialogResult.Yes)
                {
                    StartMainApplication();
                }
                else
                {
                    Program.Shutdown();
                }        
            }
        }

        public static UpdateMode GetMode()
        {
            return CurrentUpdateMode;
        }

        public static void AddModeFlag(UpdateMode flag)
        {
            CurrentUpdateMode = CurrentUpdateMode | flag;
        }

        private static void StartMainApplication()
        {
           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
            IsStarted = true;
        }

        private static void HandleArgs(string[] args)
        {
            CurrentUpdateMode = UpdateMode.EXE;
            if (args.Length == 1 && args.Contains(":"))
            {
                var ts = args.First();
                ts.Replace(":", " ");
                args = ts.Split(' ');
            }

            CurrentCmdArgs = args;
            if ((args.Length > 0) && 
                (!args.Contains(":")))
            {
                foreach(var arg in args)
                {
                    if (arg.Substring(0, 1).Equals("-"))
                        commands.Add(arg.Replace("-", string.Empty));
                    else if (arg.Substring(0, 1).Equals("+"))
                        options.Add(arg.Replace("+", string.Empty));
                }

            }
            SetupEnvironment(commands, options);
        }

        private static void SetupEnvironment(List<string> commands, List<string> options)
        {

            foreach(var command in commands)
            {
                if (command.Equals("beta"))
                    CurrentUpdateMode = CurrentUpdateMode | UpdateMode.BETA_EXE;
                if (command.Equals("nosup"))
                    CurrentUpdateMode = CurrentUpdateMode | UpdateMode.NO_SELF_UPDATE;
                if (command.Equals("r"))
                    CurrentUpdateMode = CurrentUpdateMode | UpdateMode.REINSTALL;
            }


            if(options.Count == 2)
            {
                StoreID = options.First();
                CurrentUpdateMode = CurrentUpdateMode | UpdateMode.HAS_STOREID;
                AuthString = options.Last();
                CurrentUpdateMode = CurrentUpdateMode | UpdateMode.HAS_AUTH;                
            }
            else if(options.Count == 1)
            {
                StoreID = options.First();
                CurrentUpdateMode = CurrentUpdateMode | UpdateMode.HAS_STOREID;
            }
                            
        }

        internal static void ForceDispose()
        {
            foreach (var id in DisposeList)
            {
                id.Dispose();
            }
        }

        internal static void Shutdown()
        {
            ForceDispose();


            Environment.Exit(0);
            //ExitProcess(0); 
        }

        //[DllImport("kernel32.dll")]
        //static extern void ExitProcess(uint uExitCode);

    }
}
