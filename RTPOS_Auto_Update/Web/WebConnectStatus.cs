﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.Web
{
    public enum WebConnectStatus : int
    {
        IDLE = 0,
        DOWNLOADING,
        CANCELED,
        ERRORED,
        COMPLETED


    }
}
