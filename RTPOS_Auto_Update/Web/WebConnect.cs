﻿using RTPOS_Auto_Update.UpdateEngine.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTPOS_Auto_Update.Web
{
    public class WebConnect : IDisposable
    {

        private static WebConnect _this = null;
        private static List<Thread> ThreadList = new List<Thread>();
        private static List<IDisposable> DisposeList = new List<IDisposable>();


        private Stopwatch dlsw = new Stopwatch();
        private readonly string[] SizeSuffixes =
             { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        private ProgressBar mpbar;
        private Label mlblInfo;
        
        
        public static readonly object mutex = new object();
        private WebClient webClient;
        private long totalsize;
        private long received;

        public string CurrentSavePath { get; set; }

        public delegate void WebConnectCompleteEvent(WebConnectEventArgs args);
        public event WebConnectCompleteEvent WebConnectCompletedEvent;
        
        #region Class Methods
        public static WebConnect Instance
        {
            get
            {
                lock (mutex)
                {
                    if (_this == null)
                        _this = new WebConnect();

                    return _this;
                }
            }

        }

        public WebConnect()
        {
            webClient = new WebClient();
            DisposeList.Add(webClient);
            RegisterWebClientEvents();
            
        }

        public void Dispose()
        {
            UnRegisterWebClientEvents();

            if (ThreadList.Count > 0)
                foreach (var t in ThreadList)
                    if (t.ThreadState != System.Threading.ThreadState.Aborted)
                        t.Abort();

            if (DisposeList.Count > 0)
                foreach (var d in DisposeList)
                    if (d != null)
                        d.Dispose();
        }

        private WebConnectStatus currentStatus = WebConnectStatus.IDLE;
        public WebConnectStatus CurrentWCStatus
        {
            get { return this.currentStatus; }
            private set { this.currentStatus = value; }
        }

        public void Init(ProgressBar mpbar, Label mlblInfo)
        {
            this.mpbar = mpbar;
            this.mlblInfo = mlblInfo;

            


        }

        private void ResetInstance()
        {
            this.CurrentWCStatus = WebConnectStatus.IDLE;
            received = 0;
            totalsize = 0;
        }

        private void RegisterWebClientEvents()
        {
            this.webClient.DownloadStringCompleted += WebClient_DownloadStringCompleted;
            this.webClient.DownloadProgressChanged += WebClient_DownloadProgressChanged;
            this.webClient.DownloadFileCompleted += WebClient_DownloadFileCompleted;
            this.webClient.UploadStringCompleted += WebClient_UploadStringCompleted;
            this.webClient.UploadProgressChanged += WebClient_UploadProgressChanged;
        }

        private void UnRegisterWebClientEvents()
        {
            this.webClient.DownloadStringCompleted -= WebClient_DownloadStringCompleted;
            this.webClient.DownloadProgressChanged -= WebClient_DownloadProgressChanged;
            this.webClient.DownloadFileCompleted -= WebClient_DownloadFileCompleted;
            this.webClient.UploadStringCompleted -= WebClient_UploadStringCompleted;
            this.webClient.UploadProgressChanged -= WebClient_UploadProgressChanged;
        }
        #endregion

        #region Main Methods
       


        public void UploadString(string url, string _params)
        {
            try
            {
                ResetInstance();
                var uri = new Uri(url);
                this.CurrentWCStatus = WebConnectStatus.DOWNLOADING;
                webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                webClient.UploadStringAsync(uri, _params);
            }
            catch (Exception e)
            {
                var ev = new WebConnectEventArgs(e.Message, WebConnectStatus.ERRORED, false);
                OnWebConnectEvent(ev);
            }
        }

        public void DownloadString(string url)
        {           
            try
            {
                ResetInstance();
                var uri = new Uri(url);
                this.CurrentWCStatus = WebConnectStatus.DOWNLOADING;
                webClient.DownloadStringAsync(uri);
            }
            catch (Exception e)
            {
                var ev = new WebConnectEventArgs(e.Message, WebConnectStatus.ERRORED, false);
                OnWebConnectEvent(ev);
            }
        }

        public void DownloadFile(string url, string savePath)
        {
            CurrentSavePath = savePath;
            try
            {
                ResetInstance();
                var uri = new Uri(url);
                this.CurrentWCStatus = WebConnectStatus.DOWNLOADING;
                webClient.DownloadFileAsync(uri, CurrentSavePath);
            }
            catch (Exception e)
            {
                var ev = new WebConnectEventArgs(e.Message, WebConnectStatus.ERRORED, false);
                OnWebConnectEvent(ev);
            }

        }

        private void OnWebConnectEvent(WebConnectEventArgs args)
        {
            WebConnectCompletedEvent?.Invoke(args);
        }
        #endregion




        #region WebClient Events
        private void WebClient_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            UpdateUI(e);
        }

        private void WebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            UpdateUI(e);
        }

        private void WebClient_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            var retEvent = new WebConnectEventArgs();
            if (e.Cancelled)
            {
                retEvent.Result = false;
                retEvent.Status = WebConnectStatus.CANCELED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = "Download Was Canceled.";
                OnWebConnectEvent(retEvent);
            }
            else if (e.Error != null)
            {
                retEvent.Result = false;
                retEvent.Status = WebConnectStatus.ERRORED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = e.Error.Message;
                OnWebConnectEvent(retEvent);
            }
            else
            {
                retEvent.Result = true;
                retEvent.Status = WebConnectStatus.COMPLETED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = e.Result;
                OnWebConnectEvent(retEvent);
            }
        }

        private void WebClient_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            var retEvent = new WebConnectEventArgs();
            if (e.Cancelled)
            {
                retEvent.Result = false;
                retEvent.Status = WebConnectStatus.CANCELED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = "Download Was Canceled.";
                OnWebConnectEvent(retEvent);
            }
            else if (e.Error != null)
            {
                retEvent.Result = false;
                retEvent.Status = WebConnectStatus.ERRORED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = e.Error.Message;
                OnWebConnectEvent(retEvent);
            }
            else
            {
                retEvent.Result = true;
                retEvent.Status = WebConnectStatus.COMPLETED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = CurrentSavePath;
                OnWebConnectEvent(retEvent);
            }
        }

        private void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var retEvent = new WebConnectEventArgs();


            if (e.Cancelled)
            {
                retEvent.Result = false;
                retEvent.Status = WebConnectStatus.CANCELED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = "Download Was Canceled.";
                OnWebConnectEvent(retEvent);
            }
            else if(e.Error != null)
            {
                retEvent.Result = false;
                retEvent.Status = WebConnectStatus.ERRORED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = e.Error.Message;
                OnWebConnectEvent(retEvent);
            }
            else
            {
                retEvent.Result = true;
                retEvent.Status = WebConnectStatus.COMPLETED;
                this.CurrentWCStatus = retEvent.Status;
                retEvent.Data = e.Result;
                OnWebConnectEvent(retEvent);
            }
        }
        #endregion

        #region UI UPDATE
        private void UpdateUI(DownloadProgressChangedEventArgs e)
        {
            string speed = string.Format("{0} MBs", (((e.BytesReceived / 1024d) / 1024d) / dlsw.Elapsed.TotalSeconds).ToString("0.00"));

            if (mpbar.InvokeRequired)
                mpbar.BeginInvoke((MethodInvoker)delegate () { mpbar.Value = e.ProgressPercentage; });
            else
                mpbar.Value = e.ProgressPercentage;

            string percent = string.Format("{0}%", e.ProgressPercentage.ToString());
            totalsize = e.TotalBytesToReceive;
            received += e.BytesReceived;

            string trec = SizeSuffix(e.BytesReceived);

            string tsize = SizeSuffix(e.TotalBytesToReceive);

            string str = string.Format("{0} of {1}/{2} at {3}", percent, trec, tsize, speed);

            if (mlblInfo.InvokeRequired)
                mlblInfo.BeginInvoke((MethodInvoker)delegate () { mlblInfo.Text = str; });
            else
                mlblInfo.Text = str;
        }
        private void UpdateUI(UploadProgressChangedEventArgs e)
        {
            string speed = string.Format("{0} MBs", (((e.BytesReceived / 1024d) / 1024d) / dlsw.Elapsed.TotalSeconds).ToString("0.00"));

            if (mpbar.InvokeRequired)
                mpbar.BeginInvoke((MethodInvoker)delegate () { mpbar.Value = e.ProgressPercentage; });
            else
                mpbar.Value = e.ProgressPercentage;

            string percent = string.Format("{0}%", e.ProgressPercentage.ToString());
            totalsize = e.TotalBytesToReceive;
            received += e.BytesReceived;

            string trec = SizeSuffix(e.BytesReceived);

            string tsize = SizeSuffix(e.TotalBytesToReceive);

            string str = string.Format("{0} of {1}/{2} at {3}", percent, trec, tsize, speed);

            if (mlblInfo.InvokeRequired)
                mlblInfo.BeginInvoke((MethodInvoker)delegate () { mlblInfo.Text = str; });
            else
                mlblInfo.Text = str;
        }

        private string SizeSuffix(Int64 value)
        {
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return "0.0 bytes"; }

            int mag = (int)Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }
        #endregion

        
    }
}
