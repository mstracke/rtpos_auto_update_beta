﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.UpdateEngine
{
    //Example commandline: -e -b +ezlogic +hgthfGdjsh45FghdDshhs
    [Flags]
    public enum UpdateMode : uint
    {
        NONE = 0,//Null state
        EXE = 1,//Updates RTPOS | arg: -e or no args
        HAS_STOREID = 2,//Indicates store id was passed in args | Option: +STOREID (+ezlogic)        
        BETA_EXE = 4,//Updates RTPOS EXE with Beta | arg: -b
        REINSTALL = 8,//Reinstalls | arg: -r  //Not used 7-4
        MD5VERIFY = 16,//Runs MD5 checks on all files | arg: -m //Not used 7-4
        AUTHORIZE = 32,//Uses Auth string to get URL and md5 | arg: -a
        HAS_AUTH = 64,//Flag to indicate has auth string passed in args | No args
        DEBUG = 128,//For Dev | arg -dbg //Not used currently 7-4      
        UPDATE_SELF = 256,//Flag for self updating | No args
        NO_SELF_UPDATE = 512,// Don't Update Self | arg -nosup
        POST_SELF_UPDATE = 1024//

    }
}
