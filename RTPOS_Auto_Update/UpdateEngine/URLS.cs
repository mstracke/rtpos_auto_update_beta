﻿using System;
using System.IO;


namespace RTPOS_Auto_Update.UpdateEngine
{
    public static class URLS
    {

        //WEB URLS
        public static readonly string RTPOS_EXE_DOWLOAD = "https://rtpos.com/RealtimePOS.exe";
        public static readonly string RTPOS_EXE_BETA = "https://rtpos.com/tools/ReatimePOS.exe";
        public static readonly string RTPOS_INSTALLER = "https://rtpos.com/RealtimePOSInstaller.exe";
        public static readonly string RTPOS_GET_URL = "http://bgnstudios.com/autoupdater/Downloads/GetDownload.php";
        public static readonly string RTPOS_POST_URL = "http://bgnstudios.com/autoupdater/Downloads/GetDownload.php";
        public static readonly string RTPOS_GET_URL_ZIP = "http://bgnstudios.com/autoupdater/Downloads/download_get_ZIP.php";
        public static readonly string RTPOS_POST_URL_ZIP = "http://bgnstudios.com/autoupdater/Downloads/download_post_ZIP.php";
        public static readonly string UPDATER_VERSION_URL = "http://bgnstudios.com/autoupdater/version.txt";
        public static readonly string UPDATER_DOWNLOAD_URL = "http://bgnstudios.com/autoupdater/Downloads/" + Program.CURRENT_PROCESS_NAME + ".exe";

        //Manifests
        public static readonly string GET_FILE_MANIFEST_LIST = "http://bgnstudios.com/autoupdater/Downloads/GetManifests.php";
        public static readonly string MANIFEST_BASE_URL = "http://bgnstudios.com/autoupdater/Downloads/manifests/";

        //LOCAL URLS
        //public static readonly string RTPOS_ROOT = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\RealtimePOS\");
        public static readonly string RTPOS_CONFIG = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"\RTPOSConfig\");
        public static readonly string USER_DOCUMENTS = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static readonly string WORKING_FOLDER = "update";
        public static readonly string WMI_SECURITY_ROOT = @"\\" + Environment.MachineName + @"\root\SecurityCenter";
        public static readonly string RTPOS_EXE_NAME = "RealtimePOS.exe";
        public static readonly string RTPOS_PROCESS_NAME = "RealtimePOS";
        public static readonly string RTPOS_BACKUP_EXE_NAME = "RealtimePOS_BU.exe";
        public static readonly string UNINSTALLER_FOLDER_PATH = @"C:\RealTimePOS";
        public static readonly string POS_LOG_FILE_NAME = "AU_POS_LOG.txt";
        public static readonly string MF_CHROMEPACK = "ChromePack";
        public static readonly string MF_RTPOS_ELAVON = "RTPOS_Elavon";
        public static readonly string MF_RTPOS_MAIN = "RTPOS";
        public static readonly string MF_BASE_END = "_Manifest.txt";
    }
}
