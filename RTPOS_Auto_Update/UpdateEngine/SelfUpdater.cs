﻿using RTPOS_Auto_Update.ErrorHandle;
using RTPOS_Auto_Update.UpdateEngine.Events;
using RTPOS_Auto_Update.Web;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.UpdateEngine
{
    public class SelfUpdater
    {
        private static SelfUpdater _this = null;
        
        public delegate void RenNetEventHandler(object sender, RenNetEventArgs args);
        public event RenNetEventHandler SelfUpdateEvent;

        public bool IsUpdateRunning { get; private set; }
        public bool IsRecentlyUpdated { get; private set; }

        const string renFile = "cren.exe";

        //Batch not added as backup to cren extract yet.
        const string renBatch = "bren.bat";
        private ManualResetEvent threadEvent = null;

        public Version CurrentVersion
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            }
        }

        public string ServerVersion { get; private set; }

        public SelfUpdater()
        {
            ServerVersion = null;
        }

        private void OnSelfUpdateEvent(RenNetEventArgs args)
        {
            if (threadEvent != null ||
                (!Program.GetMode().HasFlag(UpdateMode.POST_SELF_UPDATE)))
            {
                threadEvent.Set();
            }
                

            IsUpdateRunning = false;
            SelfUpdateEvent?.Invoke(this, args);
        }

        public static SelfUpdater Instance
        {
            get
            {
                if (_this == null)
                    _this = new SelfUpdater();

                return _this;
            }
        }

        

        internal void UpdateSelf(string Current_Process_Name, UpdateMode currentUpdateMode)
        {
            var args = new RenNetEventArgs();

            if (!currentUpdateMode.HasFlag(UpdateMode.NO_SELF_UPDATE))
            {
                IsUpdateRunning = true;

                if (File.Exists(Path.Combine(Program.ExecutingDirectory, renFile)))
                {
                    var renProcs = Process.GetProcessesByName(renFile.Replace(".exe", string.Empty).Trim());
                    if (renProcs.Length > 0)
                        foreach (var p in renProcs)
                            p.Kill();

                    IsRecentlyUpdated = true;
                    File.Delete(Path.Combine(Program.ExecutingDirectory, renFile));
                    IsUpdateRunning = false;
                    args.Result = REN_Result.SUCCESS_NO_UPDATE;
                    args.Data = "Recent Update with cren";
                    OnSelfUpdateEvent(args);
                }
                else if (File.Exists(Path.Combine(Program.ExecutingDirectory, renBatch)))
                {
                    IsRecentlyUpdated = true;
                    File.Delete(Path.Combine(Program.ExecutingDirectory, renBatch));
                    IsUpdateRunning = false;
                    args.Result = REN_Result.SUCCESS_NO_UPDATE;
                    args.Data = "Recent Update with Batch rename";
                    OnSelfUpdateEvent(args);

                }
                else
                {
                    IsRecentlyUpdated = false;                    
                    //May need this field at all

                    args.Result = REN_Result.NEEDS_UPDATE;
                    args.Data = "No Recent Update Detected";
                    OnSelfUpdateEvent(args); 
                }
            }
            else
            {
                args.Result = REN_Result.SUCCESS_NO_UPDATE;
                OnSelfUpdateEvent(args);
            }
           
        }

        internal void RenameSelf(string oldName, string newName)
        {
            var path = Program.ExecutingDirectory;
            var oldpath = Path.Combine(path, oldName);
            var newpath = Path.Combine(path, newName);

            var exepath = Path.Combine(path, "cren.exe");

            using(FileStream fscren = new FileStream(exepath, FileMode.CreateNew, FileAccess.Write))
            {
                byte[] bts = Resources.Getcren();
                fscren.Write(bts, 0, bts.Length);
            }

            var argstr = new StringBuilder();
            foreach (var arg in Program.CurrentCmdArgs)
                argstr.AppendFormat("{0}:", arg);

            var crenproc = new Process();
            crenproc.StartInfo.FileName = exepath;
            crenproc.StartInfo.Arguments = oldpath + " " + newpath + " " + argstr.ToString();
            crenproc.Start();            
            Program.Shutdown();


        }

        public void Update_Self(object arg)
        {
            threadEvent = arg as ManualResetEvent;            
            SelfUpdater.Instance.RunSelfUpdater();
        }

        public void RunSelfUpdater()
        {
            WebConnect.Instance.WebConnectCompletedEvent += Version_WebConnectCompletedEvent;
            WebConnect.Instance.DownloadString(URLS.UPDATER_VERSION_URL);
        }

        private void Version_WebConnectCompletedEvent(WebConnectEventArgs args)
        {
            
            WebConnect.Instance.WebConnectCompletedEvent -= Version_WebConnectCompletedEvent;
            if (args.Result && args.Status == WebConnectStatus.COMPLETED)
            {
                ServerVersion = args.Data;
                if(ServerVersion != null)
                {
                    var version = new Version(ServerVersion);
                    if(version != CurrentVersion)
                    {
                        WebConnect.Instance.WebConnectCompletedEvent += SelfDownload_WebConnectCompletedEvent;
                        WebConnect.Instance.DownloadFile(URLS.UPDATER_DOWNLOAD_URL, 
                            Path.Combine(Program.ExecutingDirectory, Program.UPDATED_PROCESS_NAME, ".exe"));

                    }
                    else
                    {
                        var evargs = new RenNetEventArgs();
                        evargs.Result = REN_Result.SUCCESS_NO_UPDATE;
                        evargs.Data = "No Self Update Needed";                       
                        OnSelfUpdateEvent(evargs);
                    }
                }
            }
            else
            {
                var evargs = new RenNetEventArgs();
                evargs.Result = REN_Result.FAILED;
                evargs.Data = "Could not get Version string from server.";
                OnSelfUpdateEvent(evargs);
            }
        }

        private void SelfDownload_WebConnectCompletedEvent(WebConnectEventArgs args)
        {

            if(args.Result)
            {
                WebConnect.Instance.WebConnectCompletedEvent -= SelfDownload_WebConnectCompletedEvent;
                var evargs = new RenNetEventArgs();
                evargs.Result = REN_Result.SUCCESS;
                evargs._ErrorCode = ErrorCode.NO_ERROR;
                evargs.Data = args.Data;
                Program.AddModeFlag(UpdateMode.POST_SELF_UPDATE);
                OnSelfUpdateEvent(evargs);
            }
            else
            {
                WebConnect.Instance.WebConnectCompletedEvent -= SelfDownload_WebConnectCompletedEvent;
                var evargs = new RenNetEventArgs();
                evargs.Result = REN_Result.FAILED;
                evargs._ErrorCode = ErrorCode.SELF_UPDATE_FAILURE;
                evargs.Data = args.Data;
                OnSelfUpdateEvent(evargs);
            }
        }
    }
}
