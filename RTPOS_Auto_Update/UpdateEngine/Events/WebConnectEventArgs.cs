﻿using RTPOS_Auto_Update.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.UpdateEngine.Events
{
    public class WebConnectEventArgs : EventArgs
    {
        public bool Result;
        public string Data;
        public WebConnectStatus Status;

        public WebConnectEventArgs() { }
        public WebConnectEventArgs(string data, WebConnectStatus status, bool result)
        {
            this.Data = data;
            this.Result = result;
            this.Status = status;
        }

    }
}
