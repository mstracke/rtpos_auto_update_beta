﻿using RTPOS_Auto_Update.ErrorHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.UpdateEngine.Events
{
    public class RenNetEventArgs
    {
        public RenNetEventArgs(REN_Result result, string data = null)
        {
            this.Result = result;
            this.Data = data;            
        }
        public RenNetEventArgs()
        {}

        public REN_Result Result;
        public string Data = null;
        public ErrorCode _ErrorCode;
    }
}
