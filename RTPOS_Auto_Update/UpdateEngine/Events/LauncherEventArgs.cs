﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.UpdateEngine.Events
{
    public class LauncherEventArgs : EventArgs
    {
        public bool Result;
        public string Data;

        public LauncherEventArgs(bool result, string data)
        {
            this.Result = result;
            this.Data = data;
        }
    }
}
