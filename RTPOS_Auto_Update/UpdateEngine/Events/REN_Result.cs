﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.UpdateEngine.Events
{
    public enum REN_Result
    {
        SUCCESS,
        SUCCESS_NO_UPDATE,
        INITIAL_FAILURE,
        FAILED,
        NEEDS_UPDATE
    }
}
