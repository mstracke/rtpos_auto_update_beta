﻿using RTPOS_Auto_Update.ErrorHandle;
using RTPOS_Auto_Update.UpdateEngine.UpdateUtils;
using RTPOS_Auto_Update.Web;
using System.IO;
using System.Threading;
using RTPOS_Auto_Update.UpdateEngine.Events;
using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace RTPOS_Auto_Update.UpdateEngine
{
    internal class POSUpdater
    {

        private static POSUpdater _this = null;
        private string mainURL = null;

        private string StoreID;
        private string AuthString;
        private bool HasStoreID;
        private bool HasAuth;
        private ManualResetEvent waitHandle;

        const string RTPOS_INSTALLER_NAME = "RealtimePOSInstaller.exe";
        const string uninstallFile = "unins000.exe";

        private string uninstallPath;
        private string LocalInstallerPath;
        private string severMd5;
        private string serverUrl;

        public POSUpdater()
        {
            this.mainURL = Program.GetMode().HasFlag(UpdateMode.BETA_EXE) ? 
                URLS.RTPOS_EXE_BETA : URLS.RTPOS_EXE_DOWLOAD;

            if (Program.GetMode().HasFlag(UpdateMode.HAS_STOREID))
            {
                this.StoreID = Program.GetStoreID();
                this.HasStoreID = true;
            }               

            if (Program.GetMode().HasFlag(UpdateMode.HAS_AUTH))
            {
                this.AuthString = Program.GetAuth();
                this.HasAuth = true;
            }
                

        }

        public static POSUpdater Instance
        {
            get
            {
                if (_this == null)
                    _this = new POSUpdater();

                return _this;
            }
        }

       

        public void Reinstall(object obj)
        {
            this.waitHandle = obj as ManualResetEvent;
            uninstallPath = Path.Combine(URLS.UNINSTALLER_FOLDER_PATH, uninstallFile);            
            WebConnect.Instance.WebConnectCompletedEvent += Instance_WebConnectCompletedEvent;
            LocalInstallerPath = Path.Combine(Program.ExecutingDirectory, URLS.WORKING_FOLDER, RTPOS_INSTALLER_NAME);
            WebConnect.Instance.DownloadFile(URLS.RTPOS_INSTALLER, LocalInstallerPath);
        }

        private void Instance_WebConnectCompletedEvent(Events.WebConnectEventArgs args)
        {
            WebConnect.Instance.WebConnectCompletedEvent -= Instance_WebConnectCompletedEvent;
            if (args.Result)
            {
                if (File.Exists(LocalInstallerPath))
                {
                    LaunchSupervisor.Instance.LaunchSupervisorCompletedEvent += Instance_LaunchSupervisorCompletedEvent;
                    LaunchSupervisor.Instance.LaunchProcess(LocalInstallerPath, true, true);
                }
            }
            else
            {
                var ec = args.Status == WebConnectStatus.CANCELED ? ErrorCode.WEB_CANCELED : ErrorCode.WEB_EXCEPTION;
                var erd = args.Status == WebConnectStatus.ERRORED ? args.Data : "Download Canceled";
                ErrorHandler.Instance.Error("Installer Download",
                    string.Format("Failed to download RTPOS installer, try again after checking internet connection, or reinstall manually. Cause: {0}", args.Data), ec);
            }
        }

        private void Instance_LaunchSupervisorCompletedEvent(Events.LauncherEventArgs args)
        {
            LaunchSupervisor.Instance.LaunchSupervisorCompletedEvent -= Instance_LaunchSupervisorCompletedEvent;
            if (args.Result)
            {
                System.Windows.Forms.MessageBox.Show(null, 
                    "RTPOS Reinstalled Successfully", 
                    "RTPOS Reinstall", 
                    System.Windows.Forms.MessageBoxButtons.OK, 
                    System.Windows.Forms.MessageBoxIcon.Information);

                LaunchSupervisor.Instance.LaunchSupervisorCompletedEvent += Success_LaunchSupervisorCompletedEvent;
                LaunchSupervisor.Instance.LaunchProcess(Path.Combine(Program.ExecutingDirectory, URLS.RTPOS_EXE_NAME), true);
            }
            else
            {
                ErrorHandler.Instance.Error("RTPOS Launch Error",
                    "Installer may have failed or errored, please try to reinstall manually.",
                    ErrorCode.POS_UPDATE_FAILURE);
                Program.Shutdown();
            }
        }

        private void Success_LaunchSupervisorCompletedEvent(LauncherEventArgs args)
        {
            LaunchSupervisor.Instance.LaunchSupervisorCompletedEvent -= Success_LaunchSupervisorCompletedEvent;
            if (args.Result)
                Program.Shutdown();
            else
            {
                ErrorHandler.Instance.Error("RTPOS Launch Error",
                    "Failed to Start RTPOS, try manually, at exceptions to AV software, or reinstall manually.  If problem presists, contact support.",
                    ErrorCode.POS_UPDATE_FAILURE);
                Program.Shutdown();
            }
        }

        public void UpdatePOS(object obj)
        {
            this.waitHandle = obj as ManualResetEvent;

            if (HasStoreID && HasAuth)
            {
                var _params = string.Format("storeid={0}&auth={1}", Program.GetStoreID(), Program.GetAuth());
                WebConnect.Instance.WebConnectCompletedEvent += POSUpdate_AuthUpload_WebConnectCompletedEvent;
                WebConnect.Instance.UploadString(URLS.RTPOS_POST_URL, _params);
            }
            else if(HasStoreID && !HasAuth)
            {

            }
            else if(!HasStoreID && !HasAuth)
            {

            }
        }

        private void POSUpdate_AuthUpload_WebConnectCompletedEvent(WebConnectEventArgs args)
        {
            WebConnect.Instance.WebConnectCompletedEvent -= POSUpdate_AuthUpload_WebConnectCompletedEvent;
            if (args.Result)
            {
                try
                {
                    var rawData = Encoding.UTF8.GetString(Convert.FromBase64String(args.Data));
                    var spl = rawData.Split('!');
                    severMd5 = spl.First();
                    serverUrl = spl.Last();

                    var posPath = Path.Combine(Program.ExecutingDirectory, 
                        URLS.WORKING_FOLDER, URLS.RTPOS_EXE_NAME);

                    WebConnect.Instance.WebConnectCompletedEvent += POS_Download_WebConnectCompletedEvent;

                    WebConnect.Instance.DownloadFile(serverUrl, posPath);

                }
                catch(Exception e)
                {
                    ErrorHandler.Instance.ExceptionedError("Error Obtaining URL from Server", 
                        ErrorCode.WEB_EXCEPTION, e);
                }
                
            }
            else
            {

            }
        }

        private void POS_Download_WebConnectCompletedEvent(WebConnectEventArgs args)
        {
            WebConnect.Instance.WebConnectCompletedEvent -= POS_Download_WebConnectCompletedEvent;
            if (args.Result)
            {

            }
            else
            {

            }
        }
    }
}