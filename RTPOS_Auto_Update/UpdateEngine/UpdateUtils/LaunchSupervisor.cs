﻿using RTPOS_Auto_Update.UpdateEngine.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.UpdateEngine.UpdateUtils
{
    public class LaunchSupervisor
    {
        private static LaunchSupervisor _this = null;
        private Process process;
        private LauncherEventArgs errorLauncherArgs;
        private bool HasError;

        public delegate void LaunchSuperEventHandler(LauncherEventArgs args);
        public event LaunchSuperEventHandler LaunchSupervisorCompletedEvent;

        public static LaunchSupervisor Instance
        {
            get
            {
                if (_this == null)
                    _this = new LaunchSupervisor();

                return _this;
            }
        }

        private string ProcessPath { get; set; }
        private bool IsSupervised { get; set; }
        private bool IsParent { get; set; }
        public string ProcessName { get; private set; }
        public string CommandLineArgs { get; private set; }

        private void OnLauncherCompletedEvent(LauncherEventArgs args)
        {
            LaunchSupervisorCompletedEvent?.Invoke(args);
        }

        public void LaunchProcess(string processPath, bool CaptureOutput, bool IsParent = false, string commandArgs = "")
        {
            this.CommandLineArgs = commandArgs;
            this.ProcessPath = processPath;
            this.IsSupervised = CaptureOutput;
            this.IsParent = IsParent;
            LaunchProcess();
                        
        }

        private void LaunchProcess()
        {
            process = new Process();
            process.StartInfo.FileName = ProcessPath;

            if (!CommandLineArgs.Equals(""))
                process.StartInfo.Arguments = CommandLineArgs;

            process.StartInfo.RedirectStandardError = IsSupervised;
            process.StartInfo.RedirectStandardInput = IsSupervised;
            process.StartInfo.RedirectStandardOutput = IsSupervised;
            process.StartInfo.UseShellExecute = !IsSupervised;
            if (IsSupervised)
            {
                process.OutputDataReceived += Process_OutputDataReceived;
                process.ErrorDataReceived += Process_ErrorDataReceived;
            }
            try
            {
                process.Start();
                this.ProcessName = process.ProcessName;

                if (IsSupervised)
                {
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();
                }
                process.WaitForInputIdle();
                Task.Delay(1500).Wait();

                if (!IsParent)
                {
                    var args = new LauncherEventArgs(process.Responding, 
                        process.Responding ? string.Empty : "Process Not Responding");

                    OnLauncherCompletedEvent(args);
                }
                else
                {
                    process.WaitForExit();
                    if (HasError)
                    {
                        OnLauncherCompletedEvent(errorLauncherArgs);
                    }
                    else
                    {
                        OnLauncherCompletedEvent(new LauncherEventArgs(true, string.Empty));
                    }
                }
                

            }
            catch (Exception e)
            {
                var msg = e.Message;
                var args = new LauncherEventArgs(false, msg);
                OnLauncherCompletedEvent(args);
            }
        }

        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!IsParent)
            {
                var msg = e.Data;
                var args = new LauncherEventArgs(false, msg);
                OnLauncherCompletedEvent(args);
            }
            else
            {
                errorLauncherArgs = new LauncherEventArgs(false, e.Data);
                HasError = true;
            }
           
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Program.Log(string.Format("{0} - {1}", ProcessName, e.Data));
        }
    }
}
