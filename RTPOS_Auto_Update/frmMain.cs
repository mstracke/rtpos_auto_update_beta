﻿
using RTPOS_Auto_Update.UpdateEngine;
using RTPOS_Auto_Update.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RTPOS_Auto_Update
{
    public partial class frmMain : Form
    {

        private Executer execr;
       

        public frmMain()
        {
            
            InitializeComponent();
            execr = new Executer();
            Program.ToDispose(execr);
        }        

        private void frmMain_Load(object sender, EventArgs e)
        {
            WebConnect.Instance.Init(MainProg, lblInfo);            
            execr.CreateExecuter(Program.GetMode());
            execr.RunExecuter();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Shutdown();                       
        }
    }
}
