﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Auto_Update.Logging
{
    public class Logger
    {
        private string LogFilePath;

        private bool IsTimeStamped = false;
        private string Timestamp;        

        public Logger(string logFile)
        {
            this.LogFilePath = Path.Combine(Program.ExecutingDirectory, logFile);
            
        }

        public void Log(string msg)
        {
            if (!IsTimeStamped)
                TimeStampLog();

            File.AppendAllText(LogFilePath, string.Format("{0}{1}", msg, Environment.NewLine));
        }

        private void TimeStampLog()
        {
            Timestamp = string.Format("Log Initiated @{0}:{1}{2}{3}{2}{2}",
                    DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), Environment.NewLine, "------------");

            File.AppendAllText(LogFilePath, Timestamp);

            IsTimeStamped = true;
        }

    }
}
