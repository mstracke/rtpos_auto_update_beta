﻿using RTPOS_Auto_Update.ErrorHandle;
using RTPOS_Auto_Update.UpdateEngine;
using RTPOS_Auto_Update.Web;
using System;
using System.Collections.Generic;
using System.Threading;

namespace RTPOS_Auto_Update
{
    internal class Executer : IDisposable
    {
        private UpdateMode CurrentUpdateMode;
        private Thread execThread;

        private List<ParameterizedThreadStart> ExecItems = new List<ParameterizedThreadStart>();
        private bool IsCreated = false;
        private ManualResetEvent execEvent;

        public void CreateExecuter(UpdateMode mode)
        {
            this.CurrentUpdateMode = mode;
            execThread = new Thread(new ThreadStart(Main_Executer));
            execEvent = new ManualResetEvent(false);

            if (!CurrentUpdateMode.HasFlag(UpdateMode.NO_SELF_UPDATE))
                if (CurrentUpdateMode.HasFlag(UpdateMode.UPDATE_SELF))
                    ExecItems.Add(SelfUpdater.Instance.Update_Self);

            
            if (CurrentUpdateMode.HasFlag(UpdateMode.REINSTALL))            
                ExecItems.Add(POSUpdater.Instance.Reinstall);

            ExecItems.Add(POSUpdater.Instance.UpdatePOS);
             
            


            IsCreated = true;
        }

        private void Main_Executer()
        {
            foreach(var exec in ExecItems)
            {  
                execEvent.Reset();
                var exth = new Thread(exec);
                exth.Start(execEvent);
                execEvent.WaitOne();
                if (IsDisposed)
                    break;
            }
        }

        public bool IsDisposed = false;

        public void Dispose()
        {
            IsDisposed = true;

            if (execThread.ThreadState != ThreadState.Aborted)
                execThread.Abort();

            execEvent.Dispose();
        }

        public void RunExecuter()
        {
            if (IsCreated)
                execThread.Start();
            else
                ErrorHandler.Instance.Error("Executer Error", "Executer Not Created", ErrorCode.EXEC_EXCEPTION);
        }
    }
}